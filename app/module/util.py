import os

basedir = os.path.abspath(os.path.dirname(__file__))
file = os.path.join(basedir, 'dictionary.txt')

def generate_words(letter):
    with open(file) as f:
        word_list = f.read().splitlines()
    words_found = ""
    for i in word_list:
        if letter in i:
            words_found += i+", "
    return words_found