# requirements
from flask import Blueprint, render_template, render_template_string, redirect, url_for
from app import env
from app.module.forms import TextForm
from app.module.util import generate_words


main_module = Blueprint('main', __name__, url_prefix='/')

title = "Word Generator"

@main_module.route("/", methods=['GET', 'POST'])
@main_module.route("/home", methods=['GET', 'POST'])
def home():
    form=TextForm()
    val=""
    if form.validate_on_submit():
        val = (env.from_string(form.text_input.data).render())
        words=generate_words(val)
        rendered_template = render_template("app.html", title=title, form=form, output=words, val=val)
        return render_template_string(rendered_template)

    rendered_template = render_template("app.html", title=title, form=form, output=val, val=val)
    return render_template_string(rendered_template)