# requirements
from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField
from wtforms.validators import DataRequired

# Text form
class TextForm(FlaskForm):
    text_input = TextAreaField('Input letter', validators=[DataRequired()])
    submit = SubmitField('Check')