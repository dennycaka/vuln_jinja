# requirements
import os
from flask import Flask
from jinja2 import Environment, FileSystemLoader

# template_dir = './templates'
env = Environment() #(loader=FileSystemLoader(template_dir))

# Initiating directory path
basedir = os.path.abspath(os.path.dirname(__file__))

# Initiating application
app = Flask(__name__)

# Application configuration
app.config['SECRET_KEY'] = '63ce3c835d310ba143c80131e0f5b5245e3151d01a583d8cb4c2f5491b937e0b'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Import a module/component using its blueprint handler variable
from app.module.controllers import main_module

# Register the route blueprints
app.register_blueprint(main_module)


